import "../styles/LandingPage.css"

import { faChevronRight, faEye, faHome, faLocationArrow, faPlay } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Container, Image, Row } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";

import Breadcrumb from "../components/Breadcrumb.js";
import Footer from "../components/Footer.js";
import NavigationBar from "../components/NavigationBar.js";
import products from "../data/ProductData.js";
import WatchingData from "../data/WatchingData.js";

const Watching = () => {
    return (
        <div className="myBG">
            <NavigationBar />
            <WatchingContent/>
            <Footer/>
        </div>
    )
}

const WatchingContent = () => {
    const {productId} = useParams();
    const product = products.find((product)=>product.id === productId);
    const {id, idDetail,  img, source, desc, title, category} = product;
return (
    <section>
        <div id="preloader">
            <div className="loader"></div>
        </div>
        <Container className="mt-5">
            <Row>
                <Col>
                    <div className="breadcrumb__links">
                        <a href="/" className="breadcrumb__logo"><FontAwesomeIcon icon={faHome}/> Beranda </a>
                        <span><FontAwesomeIcon icon={faChevronRight} className="me-1"/></span>
                        <a href={`/category/${category}`}>{category} <span><FontAwesomeIcon icon={faChevronRight} className="me-1"/></span></a>
                        <a href={`/product/${id}`}>{title} <span><FontAwesomeIcon icon={faChevronRight} className="me-1"/></span></a>
                        <span>Watch</span>
                    </div>
                </Col>
            </Row>
        </Container>
        <Container>
            <Row>
                <Col xs={12}>
                    <div className="anime__video__player mt-3">
                        <iframe width="100%" height="650" src={source} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                        {/* <video id="player" controls playsInline poster={img}>
                            <div>
                                <FontAwesomeIcon icon={faPlay}/>
                            </div>
                            <source src={videoSrc} type="video/mp4"/>
                        </video> */}
                    </div>
                    <div className="anime__details__text mb-5">
                        <div className="anime__details__title">
                            <h3>{title}</h3>
                            <p className="text-danger">1,520,347 x ditonton</p>
                        </div>
                    </div>
                    <div className="anime__details__episodes">   
                        <div className="section-title">
                            <h5>Description</h5>
                            <p style={{whiteSpace: "pre-wrap"}} className="text-white">{desc}</p>
                        </div>
                    </div>
                </Col>
            </Row>
            <CommentItem/>

        </Container>
    </section>
)
}

function CommentItem(){
return (
    <Row>
        <Col xs={8}>
            <Row>
                <Col xs={12}>
                    <div className="anime__details__episodes">
                        <div className="section-title">
                            <h5>Comment</h5>
                        </div>
                        {WatchingData.CommentWatching.map((data)=>(
                            <div className="anime__review__item">
                                <div className="anime__review__item__pic">
                                    <Image src={data.imgC}/>
                                </div>
                                <div className="anime__review__item__text">
                                    <h6>{data.name}</h6>
                                    <p>{data.comment}</p>
                                </div>
                            </div>
                        ))}
                    </div>
                </Col>
                <Col>
                    <div className="anime__details__form">
                        <div className="section-title">
                            <h5>Your Comment</h5>
                        </div>
                        <form action="#">
                            <textarea placeholder="Your Comment"></textarea>
                            <button type="submit"><FontAwesomeIcon icon={faLocationArrow}/> Submit</button>
                        </form>
                    </div>
                </Col>
            </Row>
        </Col>    
        <Col xs={1}>
            <div className="anime__details__sidebar">
                <div className="section-title">
                    <h5>Recomendation</h5>
                </div>
                {products.filter((data, i) => i < 3).map((data)=>(
                    <div className="product__sidebar__view__item set-bg">
                        <Image src={data.img} />
                        <div className="view"><FontAwesomeIcon icon={faEye}/> {data.views}</div>
                        <h5><Link to={'/'}/> </h5>
                    </div>
                ))}
            </div>
        </Col>
    </Row>
)
};


export default Watching;