import "./styles/LandingPage.css" 

import { BrowserRouter as Router, Route, Routes, } from "react-router-dom";

import Home from './page/Home';
import DetailProduct from "./page/DetailProduct";
import Watching from "./page/Watching";
import LoginPage from "./page/auth/Login";
import Signup from "./page/auth/Signup";
import Category from "./page/Category";

function App() {
  return (
      <Router basename="/">
        <Routes>
          <Route path="/" element={<Home />}/>
          <Route exact path="/dashboard" element={<Home />}/>

          <Route path="/product/:productId" element={<DetailProduct/>}/>
          <Route path='/product/:productId/:watchingId' element={<Watching/>} />
          

          <Route path="/category/:categoryId" element={<Category />}/>

          <Route path="/login" element={<LoginPage/>}/>
          <Route path="/signup" element={<Signup/>}/>
        </Routes>
      </Router>
  );
}

export default App;
